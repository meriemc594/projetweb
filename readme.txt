GUIDE D’INSTALLATION DU NEUROSHELL NFB

................................................................................................................................................................... I - A PROPOS DE NFB 						    ...................................................................................................................................................................
NFB est un outil pour organiser les informations et les activités de neurofeedback, une technique de neuromodulation. 
Il permet de conserver les informations générales à propos de personnes comme des patients et des entraîneurs et de définir 
des protocoles ciblant des régions du cerveau, ici des aires de Broadmann découpées en raison de leurs cytoarchitectures 
distinctives.

Ces protocoles doivent alors être adaptés pour rencontrer les besoins particuliers d'un patient en créant des programmes. 
Ces programmes, aux priorités variables, précisent les objectifs cliniques et sont prévus pour être exécuté en séquence. 
Des rendez-vous peuvent alors être pris pour faire des entraînements selon le chemin tracé par les programmes.

NFB permet de générer des rapports complets et détaillés concernant toutes les informations relatives à un patient. 
Il produit aussi des rapports journaliers pour signaler l'atteinte de points significatifs dans le parcours des patients.

...................................................................................................................................
II – INSTALLATION						                      UN EXEMPLE DE BANQUE DE DONNÉES EST FOURNI EN ACCOMPAGNEMENT DE NFB 
...................................................................................................................................
INSTALLATION DE PYTHON 3
Pour utiliser NFB, il est nécessaire d'installer python 3. Vous pouvez télécharger python 3 et trouver plus d'informations
à ce sujet à l'adresse suivante : https://www.python.org/downloads/

INSTALLATION DE FLASK
Pour utiliser NFB, il est aussi nécessaire d'installer Flask. Vous pouvez télécharger Flask et trouver plus d'informations 
à ce sujet à l'adresse suivante : http://flask.pocoo.org/docs/1.0/installation/#

INSTALLATION DE NFB
Pour installer NFB, copier le répertoire Projet NFB à l'emplacement souhaité. 

DESINSTALLATION
Pour désinstaller NFB, effacer le répertoire Projet NFB. Nous conseillons de conserver le 3e fichier (NFB.bd) contenant toutes 
vos données. Suivre la procédure de désinstallation de python et de Flask par la suite si vous ne voulez pas conserver ces 
applications.

IMPORTANT
Au lancement du programme, une base de données SQLite3 est créée dans le fichier NFB.bd Ce fichier contient toutes les informations
sur les activités du programme jusqu'à ce jour. Nous recommandons fortement de faire des copies de sauvegarde de votre fichier
NFB.bd De plus, comme une base de données NFB.bd est fournie en accompagnement, cette dernière doit conserver son nom et doit 
être placée dans le même répertoire que NFB_Web pour pouvoir l’explorer, mais cela empêchera la création d’une nouvelle base de 
données. Il est donc nécessaire de changer le nom de ce fichier d’accompagnement (ex. NFB_BD_EXEMPLE), de le supprimer ou de le 
déplacer afin de créer votre base de données.

Adresse à ajouter au lancement du fichier dans le navigateur : http://127.0.0.1:5000/menu_principal
