#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=                                                                                         =#
#=              		      CLASSE PERSONNE	 				   =#
#=                                                                                         =#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Personne():

	''' Constructeur '''
	def __init__ (self,identifiant,nom, prenom):
		self.__identifiant=identifiant
		self.__nom=nom
		self.__prenom=prenom

	''' Accesseurs '''
	def getIdentifiant(self):
		return self.__identifiant
	def getNom(self):
		return self.__nom
	def getPrenom(self):
		return self.__prenom

	''' Mutateurs '''
	def setNom(self,nouveau_nom):
		self.__nom=nouveau_nom
	def setPrenom(self,nouveau_prenom):
		self.__prenom=nouveau_prenom

	''' Version chaîne de caractère de l'objet '''
	def __str__(self):
		return str(self.__identifiant) +" ("+ self.__nom +" "+ self.__prenom +")"



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=                                                                                         =#
#=              		      CLASSE PATIENT	 				   =#
#=                                                                                         =#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Patient(Personne):

	''' Constructeur '''
	def __init__ (self,identifiant,nom,prenom,age,genre,casque):
		super().__init__(identifiant,nom,prenom)
		self.__age=age
		self.__genre=genre
		self.__casque=casque

	''' Accesseurs '''
	def getAgePatient(self):
		return self.__age
	def getGenrePatient(self):
		return self.__genre
	def getCasquePatient(self):
		return self.__casque

	''' Mutateurs '''
	def setAgePatient(self,nouveau_age):
		self.__age=nouveau_age
	def setGenrePatient(self,nouveau_genre):
		self.__genre=nouveau_genre
	def setCasquePatient(self,nouveau_casque):
		self.__casque=nouveau_casque

	''' Version chaine de caratères de l'objet '''
	def __str__(self):
		return "Patient " + super().__str__() + " (" + str(self.__genre)+", " + str(self.__age) +" ans, "+str(self.__casque)+")"



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=                                                                                         =#
#=              		      CLASSE ENTRAINEUR	 				   =#
#=                                                                                         =#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Entraineur(Personne):

	''' Constructeur '''
	def __init__ (self,identifiant,nom,prenom):
		super().__init__(identifiant,nom,prenom)

	''' Version chaine de caratères de l'objet '''
	def __str__(self):
		return "Entraîneur " + super().__str__()



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=                                                                                         =#
#=              		      CLASSE PROTOCOLE	 				   =#
#=                                                                                         =#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Protocole():

	''' Constructeur '''
	def __init__ (self,protocole_id,nom_protocole,aires_broadmann):
		self.__protocole_id = protocole_id
		self.__nom_protocole = nom_protocole
		self.__aires_broadmann = aires_broadmann		

	''' Accesseurs '''
	def getIdentifiantProtocole(self):
		return self.__protocole_id
	def getNomProtocole(self):
		return self.__nom_protocole
	def getAiresBroadmannProtocole(self):
		return self.__aires_broadmann

	''' Mutateurs '''
	def setNomProtocole(self,nouveau_nom_protocole):
		self.__nom_protocole = nouveau_nom_protocole
	def setAiresBroadmannProtocole(self,nouveau_aires_broadmann):
		self.__aires_broadmann = nouveau_aires_broadmann

	''' Version chaine de caratères de l'objet '''
	def __str__(self):
		return "Protocole " + str(self.__protocole_id) + " (" + self.__nom_protocole + " : " + self.__aires_broadmann + ")"



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=                                                                                         =#
#=              		      CLASSE PROGRAMME	 				   =#
#=                                                                                         =#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Programme():

	''' Constructeur '''
	def __init__ (self,programme_id,patient,protocole,priorite_clinique,nombre_seances,nom,prenom):
		self.__programme_id = programme_id
		self.__patient = patient
		self.__protocole = protocole
		self.__priorite_clinique = priorite_clinique
		self.__nombre_seances = nombre_seances
		self.__nom = nom
		self.__prenom = prenom


	''' Accesseurs '''
	def getIdentifiantProgramme(self):
		return self.__programme_id
	def getPatientProgramme(self):
		return self.__patient
	def getProtocoleProgramme(self):
		return self.__protocole
	def getPrioriteCliniqueProgramme(self):
		return self.__priorite_clinique
	def getNombreSeancesProgramme(self):
		return self.__nombre_seances
	def getNomProgramme(self):
		return self.__nom
	def getPrenomProgramme(self):
		return self.__prenom

	''' Mutateurs '''
	def setPatientProgramme(self,nouveau_patient):
		self.__patient = nouveau_patient
	def setProtocoleProgramme(self,nouveau_protocole):
		self.__protocole = nouveau_protocole
	def setPrioriteCliniqueProgramme(self,nouveau_priorite_clinique):
		self.__priorite_clinique = nouveau_priorite_clinique
	def setNombreSeancesProgramme(self,nouveau_nombre_seances):
		self.__nombre_seances = nouveau_nombre_seances
	def setNomProgramme(self,nouveau_nom):
		self.__nom = nouveau_nom
	def setPrenomProgramme(self,nouveau_prenom):
		self.__prenom = nouveau_prenom

	''' Version chaine de caratères de l'objet '''
	def __str__(self):
		return "Protocole No." + str(self.__priorite_clinique) +" - "+str(self.getProtocoleProgramme().getNomProtocole()) +" - "+ str(self.__nombre_seances) + " séances"



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=                                                                                         =#
#=              		      CLASSE ENTRAINEMENT	 			   =#
#=                                                                                         =#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Entrainement():

	''' Constructeur '''
	def __init__ (self,entrainement_id,date,heure,patient,entraineur,nom,prenom):
		self.__entrainement_id=entrainement_id
		self.__date=date
		self.__heure=heure
		self.__patient=patient
		self.__entraineur=entraineur
		self.__nom=nom
		self.__prenom=prenom

	''' Accesseurs '''
	def getIdentifiantEntrainement(self):
		return self.__entrainement_id
	def getDateEntrainement(self):
		return self.__date
	def getHeureEntrainement(self):
		return self.__heure
	def getPatientEntrainement(self):
		return self.__patient
	def getEntraineurEntrainement(self):
		return self.__entraineur
	def getNomEntrainement(self):
		return self.__nom
	def getPrenomEntrainement(self):
		return self.__prenom

	''' Mutateurs '''
	def setDateEntrainement(self,nouveau_date):
		self.__date=nouveau_date
	def setHeureEntrainement(self,nouveau_heure):
		self.__heure=nouveau_heure
	def setPatientEntrainement(self,nouveau_patient):
		self.__patient=nouveau_patient
	def setEntraineurEntrainement(self,nouveau_entraineur):
		self.__entraineur=nouveau_entraineur
	def setNomEntrainement(self,nouveau_nom):
		self.__nom = nouveau_nom
	def setPrenomEntrainement(self,nouveau_prenom):
		self.__prenom = nouveau_prenom

	''' Version chaine de caratères de l'objet '''
	def __str__(self):
		return "Entraînement :\t " + str(self.__entrainement_id) + "\nDate         :\t " + str(self.__date) + "\nHeure        :\t " + str(self.__heure) +"\nPatient      :\t " + str(self.__patient) + "\nEntraineur   :\t " + str(self.__entraineur) + "\n\n"



#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#-*             		      CLASSE RAPPORT		 			  *-#
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#


class Rapport():

	''' Constructeur '''	
	def __init__ (self,rapport_id,patient,entrainements_requis,entrainements_prevus,programmes_patient,entrainements_patient,nom,prenom):
		self.__rapport_id=rapport_id
		self.__patient=patient
		self.__entrainements_requis=entrainements_requis
		self.__entrainements_prevus=entrainements_prevus
		self.__programmes_patient=programmes_patient
		self.__entrainements_patient=entrainements_patient
		self.__nom=nom
		self.__prenom=prenom

	''' Accesseurs '''
	def getIdentifiantRapport(self):
		return self.__rapport_id
	def getPatientRapport(self):
		return self.__patient
	def getEntRequisRapport(self):
		return self.__entrainements_requis
	def getEntPrevusRapport(self):
		return self.__entrainements_prevus
	def getProgrammesRapport(self):
		return self.__programmes_patient
	def getEntrainementsRapport(self):
		return self.__entrainements_patient
	def getNomRapport(self):
		return self.__nom
	def getPrenomRapport(self):
		return self.__prenom


	''' Mutateurs '''
	def setEntRequisRapport(self,entrainements_requis2):
		self.__entrainements_requis=entrainements_requis2
	def setEntPrevusRapport(self,entrainements_prevus2):
		self.__entrainements_prevus=entrainements_prevus2
	def setProgrammesRapport(self,__programmes_patient2):
		self.__programmes_patient=__programmes_patient2
	def setEntrainementsRapport(self,entrainements_patient2):
		self.__entrainements_patient=entrainements_patient2
	def setNomRapport(self,nouveau_nom):
		self.__nom = nouveau_nom
	def setPrenomRapport(self,nouveau_prenom):
		self.__prenom = nouveau_prenom


	''' Fonctions desservant la version chaine de caratères de l'objet '''
	def strNombreQuelconque(self,x):                # Rajouter 1 zéro si inférieur a 10
		if x <10:	
			z = "0" + str(x)
			return z
		else:
			z = str(x)
			return z

	def strIDRapport(self):                         # Rajouter 1-5 zéros si inférieur a 10,100,1000,10000,100000
		x = self.getIdentifiantRapport()
		if x <10:	
			z = "00000" + str(x)
			return z
		if x < 100 and x >= 10:	
			z = "0000" + str(x)
			return z
		elif x >= 100:
			strIDRapport2(x)

	def strIDRapport2(self,x):
		if x < 1000 and x >= 100:		
			z = "000" + str(x)
			return z
		if x < 10000 and x >= 1000:		
			z = "00" + str(x)
			return z
		if x < 100000 and x >= 10000:		
			z = "0" + str(x)
			return z

	def strNPPatientRapport(self):                  # Formater le nom et le prénom d'un patient
		str_np_patient=('{:<30}{:<40}{:>9}'.format("#-*","RAPPORT PATIENT "+str(self.__rapport_id),"*-#")+"\n"+'{:<30}{:<40}{:>9}'.format("#-*",str(self.getPatientRapport().getNom().upper())+" "+str(self.getPatientRapport().getPrenom().upper()),"*-#"))
		return str_np_patient

	def strPatientRapport(self):                    # Formater le nom et le prénom d'un patient
		str_patient=str(self.getPatientRapport().getNom())+" "+str(self.getPatientRapport().getPrenom()) + " (" + str(self.getPatientRapport().getGenrePatient())+ ", "+str(self.getPatientRapport().getAgePatient()) + " ans, "+str(self.getPatientRapport().getCasquePatient())+")"
		return str_patient

	def strSeancesRapport(self):                    # Formater le nombre de séances d'un programme

		compte2                 = self.strNombreQuelconque(compte)

	def strProgrammesRapport(self):                 # Formater les programmes d'un patient
		programmes                      = ""
		for programme in self.__programmes_patient:
			programme_id            = programme.getIdentifiantProgramme()
			programme_id2           = self.strNombreQuelconque(programme_id)
			seances               = programme.getNombreSeancesProgramme()
			if seances < 10:
				seances = "0" + str(programme.getNombreSeancesProgramme())
			else:
				seances = str(programme.getNombreSeancesProgramme())
			programmes             += "".join("\n   Protocole   No." + str(programme.getPrioriteCliniqueProgramme()) + " - " + seances + " entraînements   (P" + str(programme_id2) + ")") + " - " + str(programme.getProtocoleProgramme().getNomProtocole()) 
		return programmes

	def strEntrainementsRapport(self):              # Formater les entraînements d'un patient
		entrainements                   = ""
		compte                          = 0
		for entrainement in self.__entrainements_patient:
			if entrainement !=None:
				compte                 += 1
				compte2                 = self.strNombreQuelconque(compte)
				entraineur              = entrainement.getEntraineurEntrainement().getPrenom()+" "+entrainement.getEntraineurEntrainement().getNom()
				entrainements          += "".join("\n   Entraînement #"+str(compte2)+ " - "+str(entrainement.getDateEntrainement())+" à "+str(entrainement.getHeureEntrainement()) + "h ("+str(entrainement.getIdentifiantEntrainement())+")" +" avec "+str(entraineur))
		return entrainements


	def strRendezVous(self):                        # Calculer et formater d'un patient
		entrainements_requis		= self.getEntRequisRapport()
		entrainements_prevus            = self.getEntPrevusRapport()
		rendez_vous_a_prendre=entrainements_requis-entrainements_prevus
		return str(rendez_vous_a_prendre)

	def strSeancesExcedentaires(self):              # Afficher un avis si entraînements excédentaires
		if (self.getEntRequisRapport() - self.getEntPrevusRapport()) < 0:
			return "\n\n*** AVIS IMPORTANT ***\nLe nombre d'entraînements prévus est supérieur au besoin des programmes\nSupprimer des entraînements, ajouter un programme ou modifier celui en cours"
		else:
			return ""


	''' Version chaine de caratères de l'objet '''

	def __str__(self):
		return "\n\n\n#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#\n#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#\n" + str(self.strNPPatientRapport()) + "\n#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#\n#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#\n"+"\nRAPPORT     #" + str(self.strIDRapport()) + "   " +str(self.strPatientRapport()) + "\n\nPROGRAMMES" + str(self.strProgrammesRapport()) + "\n\nENTRAINEMENTS  " +str(self.strEntrainementsRapport() +"\n\nENTRAINEMENTS REQUIS  :\t" + str(self.__entrainements_requis) + "\nENTRAINEMENTS PREVUS  :\t" + str(self.__entrainements_prevus) + "\nRENDEZ-VOUS À PRENDRE : " + str(self.strRendezVous()) +"\n" + str(self.strSeancesExcedentaires()))




#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#-*				  SUIVIS DES ENTRAINEMENTS				  *-#
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

	'''     RAPPORTS JOURNALIERS APPUYANT LE SUIVI AUPRÈS DES PATIENTS '''

	''' 	Info Générale : Sur les rendez-vous d'une journée '''	
	''' 	Avis de Suivi : Accueil d'un nouveau patient et explications de départ '''
	''' 	Avis de Suivi : Progression clinique aux séances paires >= 6 ou >10 de chaque programme '''
	''' 	Avis de Suivi : Au moment de débuter un nouveau programme d'entrainements '''

	''' *** IMPORTANT *** : Un rapport patient doit avoir été créé pour qu'un patient soit suivi '''

	# Mini-script de récupération et de transformation des données du rapport patient
	def suiviEntrainement(self,entrainement_id):                                  
		comptes_entrainements           = self.suiviEntrainementRapport()
		compte,entrainement             = self.suiviEntrainementRapport2(entrainement_id,comptes_entrainements)
		seances_programmes              = self.suiviProgrammeRapport()
		(compte,str_compte,entrainement,seances_programmes,intervalles_programmes) = self.suiviEntrainementRapport3(compte,entrainement,seances_programmes)
		self.suiviEntrainementRapport4(compte,str_compte,entrainement,seances_programmes,intervalles_programmes)

	# Attribuer un numéro à chaque séances d'entraînement
	def suiviEntrainementRapport(self):
		comptes_entrainements           = []
		compte                          = 0
		for entrainement in self.__entrainements_patient:
			compte                 += 1
			compte_entrainement     = (compte,entrainement)
			comptes_entrainements.append(compte_entrainement)
		return comptes_entrainements

	# Rapporter les entraînements numérotés
	def suiviEntrainementRapport2(self,entrainement_id,comptes_entrainements):
		for compte_entrainement in comptes_entrainements:
			compte                  = compte_entrainement[0]
			entrainement            = compte_entrainement[1]
			if entrainement_id == entrainement.getIdentifiantEntrainement():
				return compte,entrainement

	# Rapporter les programmes et calculer les entraînements requis 1
	def suiviProgrammeRapport(self):                                
		seances_programmes              = []
		cumulatif_seances               = 0
		for programme in self.__programmes_patient:
			seances=programme.getNombreSeancesProgramme()
			cumulatif_seances      += seances
			donnees_programme       = (seances,cumulatif_seances,programme)
			seances_programmes.append(donnees_programme)
		return seances_programmes	

	# Rapporter les programmes et calculer les entraînements requis 2
	def suiviEntrainementRapport3(self,compte,entrainement,seances_programmes):
		intervalles_programmes          = []
		for donnees_programme in seances_programmes:
			cumulatif_seances       = donnees_programme[1]
			intervalles_programmes.append(cumulatif_seances)
		str_compte                      = self.stringCompte(compte)
		return (compte,str_compte,entrainement,seances_programmes,intervalles_programmes)

        # Adaptation pour le rapport de versions en chaîne de caractères d'un compte quelconque
	def stringCompte(self,compte):
		if compte<10:
			str_compte = " "+ str(compte)
			return str_compte
		else:
			str_compte=str(compte)
			return str_compte

        # Distinguer la 1ère séance d'un nouveau patient de la 1ère séance d'un nouveau programme
	def suiviEntrainementRapport4(self,compte,str_compte,entrainement,seances_programmes,intervalles_programmes):
		for i in range (0,len(intervalles_programmes)):
			programme               = self.stringProgramme(i)
			if i == 0:
				self.suiviEntrainementRapport5A(i,compte,str_compte,entrainement,seances_programmes,intervalles_programmes,programme)
			elif i > 0:
				self.suiviEntrainementRapport5B(i,compte,str_compte,entrainement,seances_programmes,intervalles_programmes,programme)

	# Adaptation pour le rapport de versions en chaîne de caractères des programmes
	def stringProgramme(self,i):                    
		for j in range (0,len(self.__programmes_patient)):
			if i == j :
				programmes = self.__programmes_patient
				programme = programmes[i]
				return "Protocole No." + str(programme.getPrioriteCliniqueProgramme()) + " - " + str(programme.getProtocoleProgramme().getNomProtocole()) + " - " + str(programme.getNombreSeancesProgramme()) + " entraînements (P" + str(programme.getIdentifiantProgramme()) + ")"

	# Retourner le programme en cours de traitement pour le suivi des patients sur le web
	def retournerProgrammePourWeb(self,i): 
		for j in range (0,len(self.__programmes_patient)):
			if i == j :
				programmes = self.__programmes_patient
				programme = programmes[i]
				return programme

	# Mini-script d'affichage du rapport pour le 1er programme d'un nouveau patient
	def suiviEntrainementRapport5A(self,i,compte,str_compte,entrainement,seances_programmes,intervalles_programmes,programme):
		compte2=compte
		str_compte2=str_compte
		if compte == 1 and compte <= intervalles_programmes[i]:
			self.suiviNouveau(programme,entrainement,compte,compte2,str_compte,str_compte2)
		elif compte >= 6 and compte % 2 == 0 and compte <= intervalles_programmes[i] or compte >10 and compte <= intervalles_programmes[i]:
			self.suiviNiemeSeance(programme,entrainement,compte,compte2,str_compte,str_compte2)
		elif compte <= intervalles_programmes[i]:
			self.suiviStandard(programme,entrainement,compte,compte2,str_compte,str_compte2)

	# Mini-script d'affichage du rapport pour les programmes subséquents d'un patient
	def suiviEntrainementRapport5B(self,i,compte,str_compte,entrainement,seances_programmes,intervalles_programmes,programme):
		if compte > intervalles_programmes[i-1] and compte <= intervalles_programmes[i]:
			compte2 =compte-intervalles_programmes[i-1]
			str_compte2=self.stringCompte(compte2)					
			if compte2 == 1 and compte2 <= intervalles_programmes[i]:
				self.suiviDebut(programme,entrainement,compte,compte2,str_compte,str_compte2)
			elif int(str_compte2) >=6  and int(str_compte2) % 2 == 0 and int(str_compte2) <= intervalles_programmes[i] or int(str_compte2) >10 and int(str_compte2) <= intervalles_programmes[i]:
				self.suiviNiemeSeance(programme,entrainement,compte,compte2,str_compte,str_compte2)
			elif compte2 <= intervalles_programmes[i]:
				self.suiviStandard(programme,entrainement,compte,compte2,str_compte,str_compte2)
		

	# RAPPORT ENTRAINEMENT : ACCUEILLIR UN NOUVEAU PATIENT
	def suiviNouveau(self,programme,entrainement,compte,compte2,str_compte,str_compte2):    
		print("================================================================================")
		print (entrainement.getHeureEntrainement()+"\t\t "+entrainement.getPatientEntrainement().getNom().upper()+" "+entrainement.getPatientEntrainement().getPrenom().upper())
		print ("\t\t " + str(compte) + "er Entraînement")
		print ("             *** NOUVEAU CLIENT : EXPLICATIONS INITIALES")
		print ("\nSÉANCE #" + str(str_compte2) + "       " + str(programme))
		print ("Entraînement :   " + str(entrainement.getIdentifiantEntrainement()) + " (" + str(compte) +"e entraînement)\nPatient      :   " + str(entrainement.getPatientEntrainement()) + "\nEntraîneur   :   " + str(entrainement.getEntraineurEntrainement()) + "\n\n")


	# RAPPORT ENTRAINEMENT : SUIVI POUR ÉVALUER LA PROGRESSION AUX 2n+6 SÉANCES
	def suiviNiemeSeance(self,programme,entrainement,compte,compte2,str_compte,str_compte2):
		print("================================================================================")
		print (entrainement.getHeureEntrainement()+"\t\t "+entrainement.getPatientEntrainement().getNom().upper()+" "+entrainement.getPatientEntrainement().getPrenom().upper())	
		print ("             *** SUIVI DE LA " + str(str_compte2) + "e SÉANCE D'UN PROGRAMME D'ENTRAINEMENT")
		print ("\nSÉANCE #" + str(str_compte2) + "       " + str(programme))
		print ("Entraînement :   " + str(entrainement.getIdentifiantEntrainement()) + " (" + str(compte) +"e entraînement)\nPatient      :   " + str(entrainement.getPatientEntrainement()) + "\nEntraîneur   :   " + str(entrainement.getEntraineurEntrainement()) + "\n\n")


	# RAPPORT ENTRAINEMENT : DÉBUTER UN NOUVEAU PROGRAMME
	def suiviDebut(self,programme,entrainement,compte,compte2,str_compte,str_compte2):      
		print("================================================================================")
		print (entrainement.getHeureEntrainement()+"\t\t "+entrainement.getPatientEntrainement().getNom().upper()+" "+entrainement.getPatientEntrainement().getPrenom().upper())	
		print ("             *** DÉBUT D'UN NOUVEAU PROGRAMME D'ENTRAINEMENT")
		print ("\nSÉANCE #" + str(str_compte2) + "       " + str(programme))
		print ("Entraînement :   " + str(entrainement.getIdentifiantEntrainement()) + " (" + str(compte) +"e entraînement)\nPatient      :   " + str(entrainement.getPatientEntrainement()) + "\nEntraîneur   :   " + str(entrainement.getEntraineurEntrainement()) + "\n\n")


	# RAPPORT ENTRAINEMENT : SÉANCE SANS ÉVÈNEMENTS
	def suiviStandard(self,programme,entrainement,compte,compte2,str_compte,str_compte2):
		print ("-------------------------------------------------------------------------------")
		print (entrainement.getHeureEntrainement()+"\t\t "+entrainement.getPatientEntrainement().getNom().upper()+" "+entrainement.getPatientEntrainement().getPrenom().upper())
		print ("\nSÉANCE #" + str(str_compte2) + "       " + str(programme))
		print ("Entraînement :   " + str(entrainement.getIdentifiantEntrainement()) + " (" + str(compte) +"e entraînement)\nPatient      :   " + str(entrainement.getPatientEntrainement()) + "\nEntraîneur   :   " + str(entrainement.getEntraineurEntrainement()) + "\n\n")

